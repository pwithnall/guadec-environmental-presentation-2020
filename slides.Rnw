\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=How can I make my project more environmentally friendly?,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\sisetup{per-mode=repeated-symbol,round-precision=2,binary-units}
\DeclareSIQualifier\carbonequiv{\text{\ensuremath{\mathrm{CO_2e}}}}
\DeclareSIUnit\year{year}
\usepackage[backend=biber]{biblatex}
\addbibresource{environment.bib}
\usepackage{pmboxdraw}

% Allow additional Unicode characters in listings
\lstset{
  escapeinside={(*@}{@*)},
  literate=%
    {…}{\ldots}1%
    {●}{\textbullet}1%
    {└}{\textSFii}1%
    {─}{\textSFx}1%
}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/1/contributions/79/
%
% The use and development of GNOME has an environmental impact, and we all have
% some responsibility to measure and reduce that. This talk will give you some
% quick and easy steps for how to measure the environmental impact of your
% project, your development practices, or the wider GNOME desktop — and that’s
% the first step towards reducing that impact.
%
% For many apps, this will be as simple as finding and fixing some low-hanging
% performance issues. For many development situations, the biggest change you
% might make is to ensure your hosting provider uses renewable energy.
%
% For the wider desktop, ensuring we have appropriate default settings for power
% management, and collecting data about how GNOME machines are used will help.

\title{How can I make my project more environmentally friendly?}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{July 23, 2020}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Welcome. I’m going to talk about how you can make your project more
environmentally friendly. The aims of this talk are to:
\begin{itemize}
	\item{help you to help the environment;}
	\item{increase knowledge about where environmental costs lie, and how to avoid them;}
	\item{establish an end point for this process: how environmentally friendly do you have to make your project, and how much does it matter; and}
	\item{lay out questions which still need answers.}
\end{itemize}}

\note{Some of you may have been at
\href{https://events.gnome.org/event/1/contributions/67/}{Aditya’s talk} on the
first day of the conference. If you weren’t, I recommend checking out the
recording. While his talk looked at helping users identify applications which
are consuming power, my talk will look at helping developers reduce the
environmental impact of a specific app.}


\begin{frame}{Motivation}
\Large{Limiting global warming to \SI{1.5}{\celsius} is a global priority}
\end{frame}

\note{This is the same slide as in my talk on the environment last year, and
it’s still as relevant.

Global greenhouse gas emissions need to decline by 45\% by 2030 (9 years’ time),
and reach net zero by 2050 (29 years’ time)\cite[C.1]{sr15}. Net zero is where
all greenhouse gas emissions that can’t be eliminated are balanced by carbon
absorption in the environment (by trees) or by using as-yet-nonexistent carbon
capture technology. Throughout this talk I’ll use carbon dioxide as a proxy for
all greenhouse gases. I’ll also use energy and carbon interchangeably, as all
energy production has a carbon cost of generation (which I’ll cover shortly).

The longer it takes for emissions to reach net zero, the higher the global
average temperature will rise. So early reductions are better. Typically,
warming is expected to be greater than \SI{1.5}{\celsius} on land and less than
that on oceans\cite{sr15}.}


\begin{frame}{Life cycle analysis and products}
\begin{figure}
	\includegraphics[scale=0.5]{Life_Cycle_Thinking_Product_System.jpg}
	\caption{Life cycle analysis \small{(\href{https://commons.wikimedia.org/wiki/File:Life_Cycle_Thinking_Product_System.jpg}{public domain})}}
\end{figure}
\end{frame}

\note{Making and running software emits carbon, and in order to be able to
reduce environmental impacts, those emissions need to be measured first. One
approach for that comes from the product manufacturing industry: a life cycle
analysis (LCA) of the carbon emitted during manufacturing (said to be ‘embodied’
in the product), during use, and during disposal.

In LCA, a ‘functional unit’ is defined as a certain quantity of the product
being analysed, chosen to make the analysis well-defined, scalable and
comparable. For example, the functional unit might be a single car, produced and
driven at \SI{6}{\litre\per{}100\kilo\metre} for \SI{300000}{\kilo\metre} and
then scrapped.

A ‘system boundary’ is defined which includes all raw material inputs and
processes which are directly or indirectly needed to manufacture, transport, use
and dispose of one functional unit of the product.}

\note{In the case of a car, this would include the raw material extraction and
processing for the metals, plastics, rubber, etc. in the car; the manufacturing
of the car itself, delivery to its owner, the oil extraction and processing for
its fuel, the emissions from driving it, and the emissions (or energy and
material recovery) from scrapping it and recycling parts of it.

This kind of analysis is well understood (if not universally adopted yet) in the
manufacturing industries. It’s standardised as the ISO~14000 series.}

\note{Just like cars, software has embodied carbon costs. Those costs don’t
come from raw material extraction or burning petrol, but they come from building
and powering servers, powering networks for data transfer, and the marginal
power usage of the software on an end user’s computer (the additional power
usage compared to if the user wasn’t using that software). About the only part
of the lifecycle of software which doesn’t emit carbon is disposal.}


\begin{frame}{Carbon intensity of power generation}
\begin{figure}
	\begin{center}
		\begin{tabular}{ c r }
		Power source & Carbon intensity (\si{\gram\carbonequiv\per\kilo\watt\hour}) \\
		\hline
		Hydro    &    4 \\
		Wind     &   12 \\
		Nuclear  &   16 \\
		Solar PV &   46 \\
		Gas      &  469 \\
		Coal     & 1001 \\
		\hline
		IT average & 300
		\end{tabular}
	\end{center}
	\caption{Rough carbon intensities of power generation\cite{wiki-emission-intensity}}
\end{figure}
\end{frame}

\note{Most software carbon emissions come from generating the energy needed to
power hardware, and hence depend hugely on the carbon intensity of that power
generation (coal is carbon intensive, solar is not).

Other emissions come from the embodied costs of the hardware itself --- roughly
\SI{1.2}{\tonne\carbonequiv}\cite{goclimate-servers} to manufacture a 2019 Dell
server (not including running, transport or disposal costs). The embodied cost
of a laptop is roughly a quarter of that\cite{connell-2010-laptop}.}


\begin{frame}{Embodied carbon in software}
\Large{GNOME could provide carbon labelling for what we produce}
\end{frame}

\note{I’ve recently bought insulation for my house, and when buying it, you can
look at the datasheet and see what its lifetime carbon cost is. You can compare
insulation from different manufacturers on this basis.

I would be happy to be corrected, but I know of no analyses of the embodied
carbon costs of software.

This is an area where GNOME could improve the state of the software field. I
believe people are eventually going to want to know the carbon costs of software
they procure (especially at business/government scale). We could provide that
information, just like manufacturers of insulation do --- and just like we
already provide information about software licensing.

In order to do a rigorous analysis, there are complications like direct and
indirect rebound effects and transformational changes which would need to be
accounted for\cite[Table~1]{Court_2020}. I’ve ignored them for now, for the sake
of making \emph{some} progress.}


\begin{frame}{Functional unit and system boundary}
\Large{Functional unit: one dist tarball of a software release}
\end{frame}

\begin{frame}{Functional unit and system boundary}
\begin{figure}
	\includegraphics[scale=0.5]{system-boundary.pdf}
	\caption{Proposed system boundary for GNOME}
\end{figure}
\end{frame}

\note{So, let’s try and do a rough life cycle analysis for software in GNOME.
Here’s a functional unit (the thing we want to measure the lifetime costs of),
and a system boundary (the processes which we are directly or indirectly
responsible for which emit carbon throughout that lifetime).

For certain parts of the system, it might be easier to measure costs in
aggregate (for example, the power consumption of all the GNOME servers, or the
carbon emissions from running a conference, or the embodied cost of new server
hardware).

For other parts of the system, it might be easier to measure costs per
functional unit (for example, the resources used for continuous integration (CI)
of a particular project, or the emissions from running a hackfest for it).

As long as we’re careful to not doubly-count costs, this split should work out.}

\note{Why is it important to look at the whole system, rather than just running
profiling tools on your app? Because looking at the whole system counts the
project-wide costs and gives an incentive to reduce them which wouldn’t exist
otherwise.

That said, let’s look first at the marginal costs of an app on user systems.}


\begin{frame}{Measuring marginal costs on user systems}
\begin{itemize}
	\item{Use cases}
	\item{sysprof + Builder}
	\item{systemd unit accounting}
	\item{Kernel power state statistics}
	\item{Wattmeter on power supply}
\end{itemize}
\end{frame}

\note{The marginal costs of your app on a user’s system are all the resources
it uses while installed or running. Disk space, memory, compute time, network
bandwidth, etc. While the improvements in power usage you could make to your app
are likely small, they’re multiplied by the number of users of the app, which is
likely large. I estimate marginal costs like this are the largest environmental
impact GNOME has (see my talk last year).

There are various conventional tools for measuring and improving these marginal
costs --- but the first tool you should use is thinking. What use cases is the
app for, how does it serve them, and does it serve them in an energy-efficient
way?}


\begin{frame}{Measuring marginal costs on user systems: Use cases}
\Large{What use cases are you actually solving?}
\end{frame}

\note{For example, if your use case is allowing the user to listen to music, is
it energy-efficient to stream the music from a server on the internet all the
time, vs storing it locally?

Or if your use case is for the user to process data in some way, but your
software requires the data in a certain format which means that sometimes the
user has to spend 30 minutes manually reformatting the data first, then your
software is causing 30 minutes of extra computer use (and frustration for the
user) due to not implementing all the appropriate functionality. That costs
energy.}


\begin{frame}{Measuring marginal costs on user systems: sysprof + Builder}
\Large{\texttt{sysprof-cli -{}- your-program-here}}
\end{frame}

\begin{frame}{Measuring marginal costs on user systems: sysprof + Builder}
\begin{figure}
	\includegraphics[width=0.8\textwidth]{gnome-builder.png}
	\caption{sysprof results in GNOME Builder}
\end{figure}
\end{frame}

\note{Other tools are available to pinpoint more specific energy consumption
issues. Frequent CPU wakeups, network use and disk I/O are the biggest consumers
of energy for an app, typically.

1 hour of heavily using the CPU will use about \SI{6}{\gram\carbonequiv} (based
on a \SI{20}{\watt} swing in power usage and
\SI{300}{\gram\carbonequiv\per\kilo\watt\hour}
intensity\cite{Mahesri2004PowerCB}). \SI{1}{\giga\byte} of network traffic costs
about \SI{17}{\gram\carbonequiv}\cite{coroama-hilty-2014}.

Use sysprof from within GNOME Builder, or
\texttt{sysprof-cli} from the command line, to plot resource usage on a
timeline. There has been recent work in GLib (unreleased) and libsoup (unmerged)
to improve reporting of wakeups and network usage.}


\begin{frame}{Measuring marginal costs on user systems: systemd unit accounting}
\Large{\texttt{echo -e "DefaultCPUAccounting=yes\char`\\n"~\char`\\~\\
~~~~~~~~"DefaultIOAccounting=yes\char`\\n"~\char`\\~\\
~~~~~~~~"DefaultIPAccounting=yes" >{}>~\char`\\~\\
~~~~~~~~/etc/systemd/system.conf}}
\end{frame}

\begin{frame}[fragile]
\frametitle{Measuring marginal costs on user systems: systemd unit accounting}
\begin{lstlisting}
$ systemctl status geoclue.service 
● geoclue.service - Location Lookup Service
     Loaded: loaded (…/geoclue.service; …)
     Active: active (running) since Fri …
   Main PID: 2645 (geoclue)
         IP: 8.1M in, 3.4M out
         IO: 6.0M read, 9.1M written
      Tasks: 4 (limit: 18742)
     Memory: 10.3M
        CPU: 1min 42.217s
     CGroup: /system.slice/geoclue.service
             └─2645 /usr/libexec/geoclue
\end{lstlisting}
\end{frame}
% TODO: Highlight the IP/IO/Memory/CPU lines if possible

\note{systemd supports accounting of various resources which processes use,
including CPU time, I/O and network bandwidth. It must be turned on, but will
then be collected for all units. With the recent work to use systemd for user
sessions in GNOME, this gives reasonable (but not total) coverage of session
processes, and is a good way to get an at-a-glance look at costs and their
longer-term averages over a session.}


\begin{frame}{Measuring marginal costs on user systems: kernel power state statistics}
\Large{\texttt{sudo powertop}}
\end{frame}

\note{\texttt{powertop} is an established way of estimating the power
consumption of processes and of various bits of hardware, and can provide some
insight into the power consumption caused by wakeups from your process. These
occur whenever your process wakes up to handle input, timer events, idle
callbacks or ongoing disk I/O and network traffic.

As per Aditya’s talk at the start of the conference, we may eventually get a
breakdown like this within GNOME Usage.}


\begin{frame}{Measuring CI pipelines}
\begin{equation*}
\begin{split}
N_{\textrm{pipelines}} \times ( & \textrm{pipeline duration} \times \SI{0.114}{\kilo\watt} \times \SI{300}{\gram\carbonequiv\per\kilo\watt\hour}~+ \\
& \textrm{pipeline downloads} \times \SI{17}{\gram\carbonequiv\per\giga\byte})
\end{split}
\end{equation*}
\end{frame}
% TODO: Storage, try and automate this

\note{Moving to look at the embodied cost of a specific release of some
software, rather than the marginal cost of running it. CI is an ongoing cost of
development. Its main carbon cost is CPU time, but network bandwidth can also
end up being significant, especially if every CI pipeline downloads the
dependencies for your project from scratch.

The above formula estimates the costs of CI for a project per unit time. You can
extract the numbers from GitLab. When I calculated it for GLib, the monthly cost
was \SI{4}{\kilo\gram\carbonequiv}, which is think is not too high. However,
GLib is moderately careful to be efficient with its CI.

For comparison, the target emissions for one person for a year are
\SI{4.1}{\tonne\carbonequiv}\cite{shrinkthatfootprint}, of which this would be
1.2\%.}


\begin{frame}{Measuring the other bits}
\begin{itemize}
	\item{GNOME-administered servers}
	\item{GNOME Foundation}
	\item{Conferences}
	\item{Hackfests}
\end{itemize}
\end{frame}

\begin{frame}{Measuring the other bits}
\Large{We’re \href{https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/372}{measuring GUADEC} (thanks Bartłomiej!)}
\end{frame}

\note{Limited work has started on measuring these project-wide overheads, but
there is more work still to be done, and there’s not enough information
available to report in a talk yet.

GUADEC this year is being measured, to provide a comparison against the carbon
emissions from an in-person conference. Thanks Bartłomiej for getting the
measurements set up! I’ll be looking into the results after the conference is
over.}


\begin{frame}{Improving marginal costs on user systems}
\begin{itemize}
	\item{Where do we want to get to?}
	\item{Be used for less time}
	\item{Do less work; use less network}
	\item{Do work faster; use the network more efficiently}
	\item{Cache better}
\end{itemize}
\end{frame}

\note{Once measurement is done, improvements can happen. At what point do we
stop making improvements? Once an application consumes zero energy? Obviously
that’s not possible. My current thoughts on this are that it’s a competition:
if two pieces of software provide the same functionality, rationally users and
distros will choose the one with the lower embodied and marginal carbon costs.

These costs should be budgeted for by the user who is running the software ---
just like the carbon emissions from buying and driving a car should be budgeted
for by the driver, and car manufacturers should compete on producing cars with
low embodied and marginal emissions.

The improvements you can make to your software are all the standard ones for
performance: allow the user to work more efficiently (spending less time on the
computer); do less work, use less CPU to do it; use less network bandwidth
through compression and caching, and bunching network requests together; and the
same for disk I/O.}


\begin{frame}{Improving CI pipelines}
\begin{itemize}
	\item{Speed up your pipelines (use pre-built Docker images)}
	\item{Avoid downloads (use pre-built Docker images)}
	\item{Use shallow clones (see \href{https://tecnocode.co.uk/2020/07/09/easily-speed-up-ci-by-reducing-download-size/}{my blog})}
\end{itemize}
\end{frame}

\note{Unless your CI pipeline is run very infrequently (say, a few times a
week), you should pre-build a Docker image containing all your dependencies, and
run the CI jobs using that image.

That avoids network activity (which is carbon intensive, and can spuriously
fail) and speeds up CI jobs due to not waiting for downloads. Typically it will
speed up your CI pipeline by a factor of around 4.

Using shallow clones for cloning the git repository into a CI runner will also
give some slight speedups, but significant reductions in network use. I wrote
about this on my blog recently, so please go there for details --- fixing it is
a matter of changing a setting in GitLab.}


\begin{frame}{Improving the other bits}
\Large{More measuring to do: \href{https://gitlab.gnome.org/Teams/Board/issues/102}{Foundation operations}, and every time a hackfest is organised}
\end{frame}

\note{We’re still in the process of measuring other GNOME project overheads,
such as the cost of server infrastructure and the Foundation as an employer and
organisation. Once the measurements are done then some recommendations can be
made on the basis of the data.}


\begin{frame}{Pulling it all together}
\begin{itemize}
	\item<1->{GNOME apps should be labelled with their embodied carbon cost:
	          their share of the GNOME project and Foundation overheads,
	          plus their costs for CI and hackfests, in each major release
	          cycle}
	\item<2->{We don’t have all the data for that yet, but should collect
	          it}
	\item<3->{Reduce those embodied costs (optimise CI, make hackfests
	          carbon-neutral)}
	\item<4->{Reduce the marginal costs of your apps (optimise them, and
	          don’t waste the user’s time)}
\end{itemize}
\end{frame}

\note{So here’s how we pull it all together.}


\begin{frame}{Open questions}
\begin{enumerate}
	\item{What is the power usage of a virtualised server?}
	\item{What is the carbon intensity of our server power supplies?}
	\item{Other life cycle analysis impacts (ozone, eutrophication, water consumption, etc.)}
	\item{How many users do we have??}
	\item{Can we collect better statistics about user systems?}
\end{enumerate}
\end{frame}

\note{A lot of this analysis relies on imperfect data. We can’t wait for perfect
data before making improvements; we should improve the project, software, and
data collection in parallel.

If anybody’s got any ideas about these questions, please get in touch!}


\begin{frame}{Miscellany}
\begin{description}
	\item[Slide source]{\url{https://gitlab.com/pwithnall/guadec-environmental-presentation-2020}}
	\item[IPCC SR15 summary]{\url{https://www.ipcc.ch/sr15/chapter/spm/}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2020}}
\end{center}
\end{frame}

\note{And that’s it! Please go out there and see what you can do to improve your
applications. Mostly, it will be performance improvements and network usage
reductions. Sometimes, it might make sense to add or rearrange features to make
better use of the user’s time. Please get in touch if you have ideas, feedback
or want to discuss things further --- either about application profiling, or
about the climate crisis in general.

If you want to check my analysis, please check out the git repository linked.
The bibliography there has a number of references --- if you read just one, read
the IPCC’s SR15 report summary for policy makers\cite{sr15} (it’s short).}

\end{document}
